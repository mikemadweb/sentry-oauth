from __future__ import absolute_import, print_function

from django.conf import settings

CLIENT_ID = getattr(settings, 'OAUTH2_CLIENT_ID', None)
CLIENT_SECRET = getattr(settings, 'OAUTH2_CLIENT_SECRET', None)

AUTHORIZE_URL = getattr(settings, 'OAUTH2_AUTH_URL', None)

ACCESS_TOKEN_URL = getattr(settings, 'OAUTH2_TOKEN_URL', None)

RESOURCE = getattr(settings, 'OAUTH2_RESOURCE', None)

COMPANY_NAME = 'Company'

ERR_INVALID_RESPONSE = 'Unable to fetch user information. Please check the log.'

DATA_VERSION = '1'
