from __future__ import absolute_import, print_function

from sentry.auth.providers.oauth2 import (
    OAuth2Callback, OAuth2Provider, OAuth2Login
)

from .constants import (
    AUTHORIZE_URL, ACCESS_TOKEN_URL, CLIENT_ID, CLIENT_SECRET, DATA_VERSION,
    RESOURCE, COMPANY_NAME
)
from .views import FetchUser, OAuth2ConfigureView


class OAuth2Login(OAuth2Login):
    authorize_url = AUTHORIZE_URL
    client_id = CLIENT_ID
    relying_party_trust = RESOURCE

    def __init__(self, adfs=None):
        self.adfs = adfs
        super(OAuth2Login, self).__init__()

    def get_authorize_params(self, state, redirect_uri):
        params = super(OAuth2Login, self).get_authorize_params(
            state, redirect_uri
        )

        params['resource'] = self.relying_party_trust

        del params['scope']
        return params


class OAuth2Provider(OAuth2Provider):
    name = COMPANY_NAME
    client_id = CLIENT_ID
    client_secret = CLIENT_SECRET

    def __init__(self, adfs=None, version=None, **config):
        self.adfs = adfs
        # if adfs is not configured this is part of the setup pipeline
        # this is a bit complex in Sentry's SSO implementation as we don't
        # provide a great way to get initial state for new setup pipelines
        # vs missing state in case of migrations.
        if adfs is None:
            version = DATA_VERSION
        else:
            version = None
        self.version = version
        super(OAuth2Provider, self).__init__(**config)

    def get_configure_view(self):
        return OAuth2ConfigureView.as_view()

    def get_auth_pipeline(self):
        return [
            OAuth2Login(adfs=self.adfs),
            OAuth2Callback(
                access_token_url=ACCESS_TOKEN_URL,
                client_id=self.client_id,
                client_secret=self.client_secret,
            ),
            FetchUser(
                adfs=self.adfs,
                version=self.version,
            ),
        ]

    def get_refresh_token_url(self):
        return ACCESS_TOKEN_URL

    def build_config(self, state):
        self.ver = state['ver']
        return {
            'adfs': {
                'ver': state['ver'],
                'aud': state['aud'],
                'iss': state['iss'],
                'appid': state['appid'],
                'authmethod': state['authmethod'],
            },
            'version': DATA_VERSION,
        }

    def build_identity(self, state):
        data = state['data']
        user_data = state['user']
        return {
            'id': user_data['email'],
            'email': user_data['email'],
            'name': user_data['firstname'] + " " + user_data['lastname'],
            'data': self.get_oauth_data(data),
        }
