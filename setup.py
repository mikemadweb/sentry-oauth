#!/usr/bin/env python
"""
sentry_auth_oauth2
==================
"""
from setuptools import setup, find_packages


install_requires = [
    'sentry>=7.0.0',
]

setup(
    name='sentry_auth_oauth2',
    description='OAuth2 authentication provider for Sentry',
    long_description=__doc__,
    license='MIT',
    packages=find_packages(),
    zip_safe=False,
    install_requires=install_requires,
    extras_require={},
    include_package_data=True,
    entry_points={
        'sentry.apps': [
            'sentry_auth_oauth2 = sentry_auth_oauth2',
        ],
    },
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Topic :: Software Development'
    ],
)
